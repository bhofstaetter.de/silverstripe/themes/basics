/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/javascript/basic-bundle.js":
/*!****************************************!*\
  !*** ./src/javascript/basic-bundle.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_resp_images__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/_resp-images */ "./src/javascript/components/_resp-images.js");
/* harmony import */ var _components_resp_images__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_components_resp_images__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_resp_tables__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/_resp-tables */ "./src/javascript/components/_resp-tables.js");
/* harmony import */ var _components_resp_tables__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_components_resp_tables__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_resp_iframes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/_resp-iframes */ "./src/javascript/components/_resp-iframes.js");
/* harmony import */ var _components_resp_iframes__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_components_resp_iframes__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_navigation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/_navigation */ "./src/javascript/components/_navigation.js");
/* harmony import */ var _components_navigation__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_components_navigation__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _components_off_canvas__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/_off-canvas */ "./src/javascript/components/_off-canvas.js");
/* harmony import */ var _components_off_canvas__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_components_off_canvas__WEBPACK_IMPORTED_MODULE_4__);






/***/ }),

/***/ "./src/javascript/components/_navigation.js":
/*!**************************************************!*\
  !*** ./src/javascript/components/_navigation.js ***!
  \**************************************************/
/***/ (() => {

var NAVIGATION_ACTIVE_CLASS = 'navigation__link--sub-navigation-active';
var SUB_NAVIGATION_ACTIVE_CLASS = "sub-".concat(NAVIGATION_ACTIVE_CLASS);
var NAVIGATION_TOGGLE_ACTIVE_CLASS = 'navigation__toggle--active';
var SUB_NAVIGATION_TOGGLE_ACTIVE_CLASS = "sub-".concat(NAVIGATION_TOGGLE_ACTIVE_CLASS);

// todo: sub navigation toggles

$('html').on('click', '.navigation__toggle', function (e) {
  e.preventDefault();
  $(e.target).toggleClass(NAVIGATION_TOGGLE_ACTIVE_CLASS);
  $(e.target).closest('.navigation__link').toggleClass(NAVIGATION_ACTIVE_CLASS);
  // $(e.target).closest('.sub-navigation__link').toggleClass(SUB_NAVIGATION_ACTIVE_CLASS);
});

// $(() => {
//     window.openSectionSubNavigations();
// });

// window.openSectionSubNavigations = () => {
//     $('.navigation__toggle').each((i, toggle) => {
//         const links = $(toggle).find('.navigation__link--section.navigation__link--has-sub-navigation');
//
//         links.each((i, section) => {
//             $(this).addClass('nav__link--sub-visible');
//         });
//     });
// }

/***/ }),

/***/ "./src/javascript/components/_off-canvas.js":
/*!**************************************************!*\
  !*** ./src/javascript/components/_off-canvas.js ***!
  \**************************************************/
/***/ (() => {

$('#off-canvas').offcanvas({
  modifiers: 'right,push',
  triggerButton: '.off-canvas-toggle__open',
  closeButtonClass: "off-canvas-toggle__close",
  onOpen: function onOpen() {
    $.ajax({
      cache: true,
      crossDomain: false,
      type: 'get',
      url: "api/v1/offcanvas?page=".concat($('body').data('page-id')),
      beforeSend: function beforeSend(request, settings) {
        // todo: show loader
      },
      success: function success(data, textStatus, request) {
        $('#off-canvas .off-canvas__content').html(data);
      },
      error: function error(request, textStatus, errorThrown) {
        // todo: output NICE error message
        $('#off-canvas .off-canvas__content').html("Der Inhalt konnte nicht aufgerufen werden.<br><br><em>".concat(errorThrown, ": ").concat(textStatus, "</em>"));
      },
      complete: function complete(request, textStatus) {
        // todo: hide loader
      }
    });
  }
});

/***/ }),

/***/ "./src/javascript/components/_resp-iframes.js":
/*!****************************************************!*\
  !*** ./src/javascript/components/_resp-iframes.js ***!
  \****************************************************/
/***/ (() => {

var iframes = $('.page-text--editor iframe');
iframes.each(function (index, iframeEl) {
  $(iframeEl).data('ratio', iframeEl.height / iframeEl.width).css({
    maxWidth: "".concat(iframeEl.width, "px"),
    maxHeight: "".concat(iframeEl.height, "px")
  }).removeAttr('width').removeAttr('height');
});
$(window).resize(function () {
  iframes.each(function (index, iframeEl) {
    var iframe = $(iframeEl);
    var width = iframe.parent().width();
    iframe.width(width).height(width * iframe.data('ratio'));
  });
}).resize();

/***/ }),

/***/ "./src/javascript/components/_resp-images.js":
/*!***************************************************!*\
  !*** ./src/javascript/components/_resp-images.js ***!
  \***************************************************/
/***/ (() => {

$('.page-text--editor img').each(function (index, imageEl) {
  var image = $(imageEl);
  var width = image.attr('width');
  var height = image.attr('height');
  if (!width) {
    width = imageEl.naturalWidth;
  }
  if (!height) {
    height = imageEl.naturalHeight;
  }
  image.css({
    maxWidth: "".concat(width, "px"),
    maxHeight: "".concat(height, "px")
  });
});

/***/ }),

/***/ "./src/javascript/components/_resp-tables.js":
/*!***************************************************!*\
  !*** ./src/javascript/components/_resp-tables.js ***!
  \***************************************************/
/***/ (() => {

$('.table--responsive').each(function (index, tableEl) {
  var table = $(tableEl);
  var wrapper = $('<div class="resp-table-scroll-wrapper"></div>');
  wrapper.css({
    maxWidth: "".concat(table.width(), "px")
  });
  table.wrap(wrapper);
});

/***/ }),

/***/ "./src/scss/basic-bundle.scss":
/*!************************************!*\
  !*** ./src/scss/basic-bundle.scss ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/dist/javascript/basic-bundle": 0,
/******/ 			"dist/css/basic-bundle": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["dist/css/basic-bundle"], () => (__webpack_require__("./src/javascript/basic-bundle.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["dist/css/basic-bundle"], () => (__webpack_require__("./src/scss/basic-bundle.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;