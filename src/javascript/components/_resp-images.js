$('.page-text--editor img').each((index, imageEl) => {
    const image = $(imageEl);

    let width = image.attr('width');
    let height = image.attr('height');

    if (!width) {
        width = imageEl.naturalWidth;
    }

    if (!height) {
        height = imageEl.naturalHeight;
    }

    image.css({
        maxWidth: `${width}px`,
        maxHeight: `${height}px`,
    });
});
