$('.table--responsive').each((index, tableEl) => {
    const table = $(tableEl);
    const wrapper = $('<div class="resp-table-scroll-wrapper"></div>');

    wrapper.css({
        maxWidth: `${table.width()}px`,
    });

    table.wrap(wrapper);
});
