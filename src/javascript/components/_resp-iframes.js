const iframes = $('.page-text--editor iframe');

iframes.each((index, iframeEl) => {
    $(iframeEl)
        .data('ratio', iframeEl.height / iframeEl.width)
        .css({
            maxWidth: `${iframeEl.width}px`,
            maxHeight: `${iframeEl.height}px`,
        })
        .removeAttr('width')
        .removeAttr('height');
});

$(window).resize(() => {
    iframes.each((index, iframeEl) => {
        const iframe = $(iframeEl);
        const width = iframe.parent().width();

        iframe
            .width(width)
            .height(width * iframe.data('ratio'));
    });
}).resize();
