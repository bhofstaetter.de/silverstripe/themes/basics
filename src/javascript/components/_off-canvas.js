$('#off-canvas').offcanvas({
    modifiers: 'right,push',
    triggerButton: '.off-canvas-toggle__open',
    closeButtonClass: "off-canvas-toggle__close",
    onOpen: function() {
        $.ajax({
            cache: true,
            crossDomain: false,
            type: 'get',
            url: `api/v1/offcanvas?page=${$('body').data('page-id')}`,
            beforeSend: function (request, settings) {
                // todo: show loader
            },
            success: function (data, textStatus, request) {
                $('#off-canvas .off-canvas__content').html(data);
            },
            error: function (request, textStatus, errorThrown) {
                // todo: output NICE error message
                $('#off-canvas .off-canvas__content').html(
                    `Der Inhalt konnte nicht aufgerufen werden.<br><br><em>${errorThrown}: ${textStatus}</em>`
                );
            },
            complete: function (request, textStatus) {
                // todo: hide loader
            }
        });
    },
});
