const NAVIGATION_ACTIVE_CLASS = 'navigation__link--sub-navigation-active';
const SUB_NAVIGATION_ACTIVE_CLASS = `sub-${NAVIGATION_ACTIVE_CLASS}`;
const NAVIGATION_TOGGLE_ACTIVE_CLASS = 'navigation__toggle--active';
const SUB_NAVIGATION_TOGGLE_ACTIVE_CLASS = `sub-${NAVIGATION_TOGGLE_ACTIVE_CLASS}`;

// todo: sub navigation toggles

$('html').on('click', '.navigation__toggle', (e) => {
    e.preventDefault();
    $(e.target).toggleClass(NAVIGATION_TOGGLE_ACTIVE_CLASS);
    $(e.target).closest('.navigation__link').toggleClass(NAVIGATION_ACTIVE_CLASS);
    // $(e.target).closest('.sub-navigation__link').toggleClass(SUB_NAVIGATION_ACTIVE_CLASS);
});

// $(() => {
//     window.openSectionSubNavigations();
// });

// window.openSectionSubNavigations = () => {
//     $('.navigation__toggle').each((i, toggle) => {
//         const links = $(toggle).find('.navigation__link--section.navigation__link--has-sub-navigation');
//
//         links.each((i, section) => {
//             $(this).addClass('nav__link--sub-visible');
//         });
//     });
// }
