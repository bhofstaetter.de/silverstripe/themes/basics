const mix = require('laravel-mix');

mix
    .js('src/javascript/basic-bundle.js', 'dist/javascript')
    .sass('src/scss/basic-bundle.scss', 'dist/css')
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.scss/,
                    loader: 'import-glob-loader'
                }
            ]
        }
    });
