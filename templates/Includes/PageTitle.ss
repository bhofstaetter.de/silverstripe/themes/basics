<% if $ShowTitle || $ShowBreadcrumbs %>
    <div class="page-title">
        <% if $ShowTitle %>
            <div class="page-title__title">
                <h1><span>$Title</span></h1>
            </div>
        <% end_if %>
        <% if $ShowBreadcrumbs %>
            <div class="page-title__breadcrumbs">
                $Breadcrumbs
            </div>
        <% end_if %>
    </div>
<% end_if %>
