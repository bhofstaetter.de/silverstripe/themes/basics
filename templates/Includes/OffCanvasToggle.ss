<div class="off-canvas-toggle">
    <% if $close %>
        <a class="off-canvas-toggle__close">
            <span>Navigation schließen</span>
        </a>
    <% else %>
        <a class="off-canvas-toggle__open">
            <span>Navigation öffnen</span>
        </a>
    <% end_if %>
</div>
