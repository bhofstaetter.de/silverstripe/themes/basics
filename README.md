# Silverstripe Basic Theme

## Requirements

```yaml
PageController:
    yamReq_javascript:
        'https://unpkg.com/js-offcanvas@1/dist/_js/js-offcanvas.pkgd.min.js':
            identifier: 'js-offcanvas'
            include: 'single'
            version: '1.99.99'
            order: 5
        'dist/javascript/basic-bundle.js':
            identifier: 'basic-theme'
            resolve: 'theme'
            include: 'single'
            order: 10 # before custom theme
    yamReq_css:
        dist/css/basic-bundle.css:
            identifier: 'basic-theme'
            resolve: 'theme'
            include: 'combined'
            order: 0
        'https://unpkg.com/js-offcanvas@1dist/_css/prefixed/js-offcanvas.css':
            identifier: 'js-offcanvas'
            include: 'single'
            version: '1.99.99'
```

## Todo

- Google Analytics Include
- Cookiebot Include
- Loader css?
- Pagination css?
- navigation css?
- build production ready js/css
